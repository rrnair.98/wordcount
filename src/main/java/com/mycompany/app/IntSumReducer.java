package com.mycompany.app;

import java.io.IOException;
import java.util.concurrent.atomic.LongAdder;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.io.Text;

public class IntSumReducer extends Reducer<Text, IntWritable, Text, IntWritable>{

    private static IntWritable intWritableResult = new IntWritable();

    @Override
    public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException{
        LongAdder longAdder = new LongAdder();
        //int sum = 0;
        values.forEach(e -> longAdder.add(e.get()));
        intWritableResult.set(longAdder.intValue());
        context.write(key, intWritableResult);
    }
}